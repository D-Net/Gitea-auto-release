# Gitea Auto release
_author: Sandro La Bruzzo_


## Description
This python script automatically creates a release on a Gitea repository  using the [swagger api](https://docs.gitea.io/en-us/api-usage/) provided by Gitea.
It requires the `token API` you can generate through the gitea user interface.


## Installation

` pip install -r requirements.txt`


## Usage

`python gitea_release.py --property gitea.properties_PATH -token ATOKEN`


It requires a property file containing all the info needed to generate the Release and attach a `tar.gz` of your build folder to the Release.

below an example of the property file (that you can include inside your git repository)

## Release Property File

```
#The name of the tag
tag_name= TAG_3.0 
# A description of the tag
tag_description= Tag 3.0
#The name of your branch you want to release
target=main
#The release name
release_name= Release_3.0
#The release description
release_description= This is a release 3.0
#The path of your build folder (this folder will be compressed in a tar.gz file)
build_path=/Users/A/PATH/OF/BUILD
#The name of the attachment 
attachment_name= release
# The name of your gitea swagger api repo
gitea_repo=https://code-repo.d4science.org/api/v1
# The repository name you want to release
repo_name = openaire-graph-docs
#The user from whom the token was generated
repo_user=sandro.labruzzo
```




Enjoy